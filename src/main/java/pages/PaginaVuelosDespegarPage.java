package pages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import util.Utilidades;

public class PaginaVuelosDespegarPage {
	WebDriver driver;
	Utilidades utilidades;

	public PaginaVuelosDespegarPage(WebDriver driver) throws IOException {
		this.driver = driver;
		utilidades = new Utilidades();
	}

	public void ObtenerInfoVuelos() throws InterruptedException, IOException {
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		List<Double> lista = new ArrayList<Double>();
		utilidades.esperar(5000);
		for (int i = 1; i < 8; i++) {
			String USER_XPATH = ".//span[%s]/span/cluster/div/div/span/fare/span/span/main-fare/span/span[2]/flights-price/span/flights-price-element/span/span/em/span[2]";
			String fullXpath = String.format(USER_XPATH, i);

			String valor = driver.findElement(By.xpath(fullXpath)).getText();
			String valor2 = valor.replace(".", "");
			lista.add(Double.parseDouble(valor2));
		}
		Collections.sort(lista);
		utilidades.CrearLibro(utilidades.propiedades.getProperty("Libro") + Math.random()
				+ utilidades.propiedades.getProperty("ExtLibro"), lista);
	}

	
	
	
	
}
