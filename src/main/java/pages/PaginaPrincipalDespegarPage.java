package pages;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import util.Utilidades;

public class PaginaPrincipalDespegarPage {
	WebDriver driver;
	Utilidades utilidades;

	public PaginaPrincipalDespegarPage(WebDriver driver) throws IOException {
		this.driver = driver;
		utilidades = new Utilidades();
	}

	public boolean VentanaEmergenteExiste() {
		try {
			driver.findElement(By.xpath("/html/body/div[16]/div/div[1]/div/h3")).isDisplayed();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public void CerrarVentanaEmergente() {
		driver.findElement(By.xpath("/html/body/div[16]/div/div[1]/span")).click();
	}

	public void SeleccionarVuelos() throws InterruptedException {
		Thread.sleep(1000);
		driver.findElement(By.xpath("/html/body/div[8]/div[2]/div/ul/li[2]/a")).click();
	}

	public void IngresarCiudadOrigen(String origen) throws InterruptedException {

		driver.findElement(By.xpath(
				"//*[@id=\"searchbox-sbox-all-boxes\"]/div/div/div/div[3]/div[2]/div[1]/div[1]/div/div[1]/div/div/div/input"))
				.clear();

		driver.findElement(By.xpath(
				"//*[@id=\"searchbox-sbox-all-boxes\"]/div/div/div/div[3]/div[2]/div[1]/div[1]/div/div[1]/div/div/div/input"))
				.sendKeys(origen);
		utilidades.esperar(1000);
		driver.findElement(By.xpath(
				"//*[@id=\"searchbox-sbox-all-boxes\"]/div/div/div/div[3]/div[2]/div[1]/div[1]/div/div[1]/div/div/div/input"))
				.sendKeys(Keys.ENTER);

	}

	public void IngresarCiudadDestino(String destino) throws InterruptedException {
		utilidades.esperar(1000);
		driver.findElement(By.xpath(
				"//*[@id=\"searchbox-sbox-all-boxes\"]/div/div/div/div[3]/div[2]/div[1]/div[1]/div/div[2]/div/div/div/div/input"))
				.sendKeys(destino);
		utilidades.esperar(1000);
		driver.findElement(By.xpath(
				"//*[@id=\"searchbox-sbox-all-boxes\"]/div/div/div/div[3]/div[2]/div[1]/div[1]/div/div[2]/div/div/div/div/input"))
				.sendKeys(Keys.ENTER);
	}

	public void seleccionarFechaInicial(String fechaInicial) throws InterruptedException {

		driver.findElement(By.xpath(
				"//*[@id=\"searchbox-sbox-all-boxes\"]/div/div/div/div[3]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]"))
				.click();
		utilidades.esperar(1000);
		seleccionarFecha(LocalDate.now().with(TemporalAdjusters.firstDayOfMonth()),
				LocalDate.parse(fechaInicial, DateTimeFormatter.ofPattern(utilidades.propiedades.getProperty("FormatoFecha"))), Boolean.FALSE);

	}

	public void seleccionarFechaRegreso(String fechaSalida, String fechaRegreso) throws InterruptedException {
		utilidades.esperar(1000);
		seleccionarFecha(
				LocalDate.parse(fechaSalida, DateTimeFormatter.ofPattern(utilidades.propiedades.getProperty("FormatoFecha")))
						.with(TemporalAdjusters.firstDayOfMonth()),
				LocalDate.parse(fechaRegreso, DateTimeFormatter.ofPattern(utilidades.propiedades.getProperty("FormatoFecha"))), Boolean.TRUE);

	}

	public void SeleccionarNroPersonas(int NumPersonas) throws InterruptedException {

		driver.findElement(By.xpath(
				"//*[@id=\"searchbox-sbox-all-boxes\"]/div/div/div/div[3]/div[2]/div[1]/div[2]/div[2]/div[6]/div[2]/div"))
				.click();
		Thread.sleep(1000);
		for (int i = 1; i < NumPersonas; i++) {
			driver.findElement(By.xpath("/html/body/div[3]/div/div[1]/div[2]/div/div[1]/div/div[1]/div[2]/div/a[2]"))
					.click();
		}

		driver.findElement(By.xpath("/html/body/div[3]/div/div[2]/a")).click();
	}

	public void ClicBotonBuscar() {
		driver.findElement(By.xpath("//*[@id=\"searchbox-sbox-all-boxes\"]/div/div/div/div[3]/div[2]/div[4]/div/a/em"))
				.click();
	}
	
	
	public String mensajeDestinoNoValido() {
		return driver.findElement(By.xpath("//*[@id=\"searchbox-sbox-all-boxes\"]/div[2]/div/div/div[3]/div[2]/div[1]/div[1]/div/div[2]/div/div/div/div/span[2]")).getText();
	}
		
	
	public void seleccionarFecha(LocalDate fechaRef, LocalDate fechaASeleccionar, Boolean isFechaRegreso)
			throws InterruptedException {

		Integer meses = Period.between(fechaRef, fechaASeleccionar).getMonths();
		Integer dia = fechaASeleccionar.getDayOfMonth();

		if (meses >= 1) {
			for (int i = 0; i < meses; i++) {
				driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div[2]")).click();
			}
		}
		String USER_XPATH = "/html/body/div[4]/div/div[4]/div[%s]/div[4]/span[%s]";
		if (isFechaRegreso) {
			Integer xpathRegreso = Period
					.between(LocalDate.now().with(TemporalAdjusters.firstDayOfMonth()), fechaASeleccionar).getMonths();
			String fullXpath = String.format(USER_XPATH, xpathRegreso + 1, dia);
			driver.findElement(By.xpath(fullXpath)).click();
		} else {

			String fullXpath = String.format(USER_XPATH, meses + 1, dia);
			driver.findElement(By.xpath(fullXpath)).click();
		}
	}
}
