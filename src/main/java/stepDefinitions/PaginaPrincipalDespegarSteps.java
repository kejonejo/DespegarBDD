package stepDefinitions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.http.util.Asserts;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.PaginaPrincipalDespegarPage;
import pages.PaginaVuelosDespegarPage;
import util.Utilidades;

public class PaginaPrincipalDespegarSteps {

	WebDriver driver;
	PaginaPrincipalDespegarPage paginaPrincipalDespegarPage;
	PaginaVuelosDespegarPage paginaVuelosDespegarPage;
	Utilidades utilidades;


	@Before
	public void setUp() throws IOException {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		paginaPrincipalDespegarPage = new PaginaPrincipalDespegarPage(driver);
		paginaVuelosDespegarPage = new PaginaVuelosDespegarPage(driver);
		utilidades = new Utilidades();
	}

	@Given("el usuario esta en la pagina de despegar")
	public void usuarioEstaEnPaginaDespegar() {

		//driver.get("https://www.despegar.com.co");
		driver.get(utilidades.propiedades.getProperty("URL"));
		if (paginaPrincipalDespegarPage.VentanaEmergenteExiste()) {
			paginaPrincipalDespegarPage.CerrarVentanaEmergente();
		}
	}

	@When("el usuario selecciona la opcion vuelos")
	public void usuarioSeleccionaOpcionVuelos() throws InterruptedException {
		paginaPrincipalDespegarPage.SeleccionarVuelos();
	}

	@And("^el usuario ingresa el origen (.*)$")

	public void usuarioIngresaOrigen(String origen) throws InterruptedException {
		paginaPrincipalDespegarPage.IngresarCiudadOrigen(origen);
	}

	@And("^el usuario ingresa el destino (.*)$")
	public void usuarioIngresaDestino(String destino) throws InterruptedException {
		paginaPrincipalDespegarPage.IngresarCiudadDestino(destino);

	}

	@And("^el usuario selecciona fecha salida (.*)$")
	public void usuarioSeleccionaFechaSalida(String fechaSalida) throws InterruptedException {

		paginaPrincipalDespegarPage.seleccionarFechaInicial(fechaSalida);
	}

	@And("^el usuario selecciona fecha regreso (.*) y (.*)$")
	public void usuarioSeleccionaFechaRegreso(String fechaSalida, String fechaRegreso) throws InterruptedException {
		paginaPrincipalDespegarPage.seleccionarFechaRegreso(fechaSalida, fechaRegreso);
	}

	@And("el usuario selecciona pasajeros (.*)")
	public void UsuarioSeleccionaPasajeros(int NumPersonas) throws InterruptedException {
		paginaPrincipalDespegarPage.SeleccionarNroPersonas(NumPersonas);

	}

	@And("el usuario selecciona boton buscar")
	public void usuarioSeleccionaBotonBuscar() {
		paginaPrincipalDespegarPage.ClicBotonBuscar();

	}

	@Then("el usuario guarda la informacion de los vuelos")
	public void UsuarioGuardaInfoVuelos() throws InterruptedException, IOException {
		paginaVuelosDespegarPage.ObtenerInfoVuelos();
	}
	
	@Then ("el usuario valida el mensaje de error guarda la informacion de los vuelos (.*)$")
	public void UsuarioValidaMensaje(String Mensaje) {
		
		assertEquals(Mensaje, paginaPrincipalDespegarPage.mensajeDestinoNoValido().toUpperCase().contains(Mensaje.toUpperCase()), false);		
	}
	 @After
	
	 public void CerrarPagina() {
	 driver.close();
	 }

}
