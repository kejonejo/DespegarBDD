package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.HSSFColor.HSSFColorPredefined;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Utilidades {
	public Properties propiedades;

	public Utilidades() throws IOException {
		propiedades = new Properties();
		cargarProperties();
		
	}

	public void CrearLibro(String ruta, List<Double> Lista) throws IOException {
		HSSFWorkbook libroCopiar = new HSSFWorkbook();
		HSSFSheet hoja1 = libroCopiar.createSheet("InfoTarjetas");
		HSSFRow fila = hoja1.createRow(0);

		for (int i = 0; i < Lista.size(); i++) {
			HSSFCell celda = fila.createCell(0);
			celda.setCellValue(Lista.get(i));
			fila = hoja1.createRow(i + 1);
		}
		
		HSSFCellStyle styleGroup1 = libroCopiar.createCellStyle();
		styleGroup1.setFillPattern( FillPatternType.SOLID_FOREGROUND);
		styleGroup1.setFillForegroundColor(IndexedColors.GREEN.index);
		hoja1.getRow(0).getCell(0).setCellStyle(styleGroup1);

		
		FileOutputStream os = new FileOutputStream(new File(ruta));

		libroCopiar.write(os);
		os.close();
	}

	public void esperar(long seg) throws InterruptedException {
		Thread.sleep(seg);
	}


	public  void cargarProperties() throws IOException {
		InputStream archivo=new FileInputStream("DatosComunes.properties");
			propiedades.load(archivo);
			archivo.close();
	}
	
}
