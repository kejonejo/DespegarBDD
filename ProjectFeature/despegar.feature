Feature: Reserva Despegar
  Yo como usuario recurrente quiero realizar la reserva en despegar
    
  Scenario Outline: Reserva exitosa
    Given el usuario esta en la pagina de despegar
    When el usuario selecciona la opcion vuelos
    When el usuario ingresa el origen <origen>
    And el usuario ingresa el destino <destino>
    And el usuario selecciona fecha salida <fechaSalida>
    And el usuario selecciona fecha regreso <fechaSalida> y <fechaRegreso>
    And el usuario selecciona pasajeros <NumPersonas>
    And el usuario selecciona boton buscar
    Then el usuario guarda la informacion de los vuelos
    Examples:
    |origen|destino|fechaSalida|fechaRegreso|NumPersonas|
    |Medellin, Antioquia, Colombia|Cartagena de Indias|09/1/2018|02/20/2019|6|
    
    
    Scenario Outline: Reserva fallida ciudad destino igual a ciudad origen
    Given el usuario esta en la pagina de despegar
    When el usuario selecciona la opcion vuelos
    When el usuario ingresa el origen <origen>
    And el usuario ingresa el destino <destino>
    And el usuario selecciona fecha salida <fechaSalida>
    And el usuario selecciona fecha regreso <fechaSalida> y <fechaRegreso>
    And el usuario selecciona pasajeros <NumPersonas>
    And el usuario selecciona boton buscar
    Then el usuario valida el mensaje de error guarda la informacion de los vuelos <MensajeError>
    Examples:
    |origen|destino|fechaSalida|fechaRegreso|NumPersonas|MensajeError|
    |Medellin, Antioquia, Colombia|Medellin, Antioquia, Colombia|09/1/2018|02/20/2019|6|El destino debe ser diferente del origen|